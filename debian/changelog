ruby-commandline (0.7.10-14) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 official URL for Format field
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 03:48:49 +0530

ruby-commandline (0.7.10-13) unstable; urgency=low

  [ Gunnar Wolf ]
  * Dropped Esteban Manchado from the maintainers, as he is retiring from
    the project. Thanks for all the fish!

  [ Paul van Tilburg ]
  * debian/control:
    - Added myself to the uploaders.
    - Dropped the depend on ruby-commandline for ruby-commandline-doc.
    - Ensure that the priority of the transitional packages is "extra".
    - Bumped standards version to 3.9.3; no changes required.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 29 May 2012 22:45:47 +0200

ruby-commandline (0.7.10-12) unstable; urgency=low

  * Add missing dependency on ruby-text-format
  * Update dependencies of ruby-commandline-doc

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Mon, 17 Oct 2011 01:23:13 +0200

ruby-commandline (0.7.10-11) unstable; urgency=low

  * Conversion to gem2deb
    + rename source and binary packages
  * Add myself as an uploader
  * Override duplicate description for transitional packages
  * Bumped standards version to 3.9.2

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Fri, 30 Sep 2011 17:53:34 +0200

libcommandline-ruby (0.7.10-10) unstable; urgency=low

  * Drop open4.rb from the package, and add dependency for libopen4-ruby1.8
    (Closes: #479502).
  * Fix doc-base section.

 -- Esteban Manchado Velázquez <zoso@debian.org>  Mon, 05 May 2008 21:54:56 +0200

libcommandline-ruby (0.7.10-9) unstable; urgency=low

  * Fix patch for the COLUMNS environment variable fix.

 -- Esteban Manchado Velázquez <zoso@debian.org>  Sun, 04 Nov 2007 23:03:59 +0100

libcommandline-ruby (0.7.10-8) unstable; urgency=low

  [ Esteban Manchado Velázquez ]
  * Patched to avoid a problem with the COLUMNS environment variable
    (Closes: #447789)

 -- Esteban Manchado Velázquez <zoso@debian.org>  Sat, 27 Oct 2007 19:04:01 +0200

libcommandline-ruby (0.7.10-7) unstable; urgency=low

  [ Esteban Manchado Velázquez ]
  * Fixed maintainer email address.

  [ Paul van Tilburg ]
  * Adapted debian/control, debian/rules, and remove debian/control.in to
    drop the Uploaders rule.
  * Bumped standards version to 3.7.2; no changes required.
  * Moved the build-depend-indep on ruby-pkg-tools to the build-depends
    for it is used in the clean target.
  * Added myself to the Uploaders field.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 19 Jun 2007 11:03:50 +0100

libcommandline-ruby (0.7.10-6) unstable; urgency=low

  * Added watch file
  * Adapted to ruby-pkg-tools 0.8
  * Added dependency on libtext-format-ruby1.8
  * Moved cdbs and debhelper from Build-Depends-Indep to Build-Depends (to
    avoid lintian warning)
  * Removed CVS directories from the libcommandline-ruby-doc binary package
    (avoids another lintian warning)

 -- Esteban Manchado Velázquez <zoso@debian.org>  Wed, 26 Apr 2006 23:30:33 +0100

libcommandline-ruby (0.7.10-5) unstable; urgency=low

  [ Lucas Nussbaum ]
  * Add build-dependancy on graphviz, so the docs are properly
    generated.

  [ Paul van Tilburg ]
  * Moved upstream documentation from libcommandline-ruby1.8 to
    libcommandline-ruby-doc.
  * Added the examples to libcommandline-ruby-doc.
  * Cleaned up the upstream tarball (removed CVS dirs).

  [ Esteban Manchado Velázquez ]
  * Set myself as "main" maintainer.
  * Fixed package sections.

 -- Esteban Manchado Velázquez <zoso@debian.org>  Thu, 30 Mar 2006 00:15:38 +0100

libcommandline-ruby (0.7.10-4) unstable; urgency=low

  * Added libcommandline-ruby-doc package, with documentation.

 -- Esteban Manchado Velázquez <zoso@debian.org>  Thu, 12 Jan 2006 00:07:14 +0000

libcommandline-ruby (0.7.10-3) unstable; urgency=low

  * Fixed Uploaders: and Maintainer: control fields. It also means having
    debian/control in UTF-8 (Closes: #340997).
  * Changed Build-Depends-Indep on debhelper to >= 4.1.0, as it's needed to
    use CDBS and debhelper.mk.
  * Changed Section: to interpreters.
  * Simplified a bit debian/rules.

 -- Esteban Manchado Velázquez <zoso@debian.org>  Sat,  3 Dec 2005 22:53:07 +0000

libcommandline-ruby (0.7.10-2) unstable; urgency=low

  * The "how could you?" release.
  * Added mandatory field "Files" in the doc-base files (Closes: #340376).

 -- Esteban Manchado Velázquez <zoso@debian.org>  Wed, 23 Nov 2005 11:49:26 +0000

libcommandline-ruby (0.7.10-1) unstable; urgency=low

  [ Esteban Manchado Velázquez ]
  * Initial upload (Closes: #338634)

 -- Esteban Manchado Velázquez <zoso@debian.org>  Fri, 11 Nov 2005 17:47:49 +0000

